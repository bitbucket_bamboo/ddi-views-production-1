<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template name="markDown2html">
        <xsl:param name="pString" select="."/>
        <xsl:analyze-string select="$pString" 
            regex="(#(.*)#&#xA;)|((- (.*)&#xA;)+)">
            <xsl:matching-substring>
                <xsl:choose>
                    <xsl:when test="regex-group(1)">
                        <h1>
                            <xsl:call-template name="inline">
                                <xsl:with-param name="pString" 
                                    select="regex-group(2)"/>
                            </xsl:call-template>
                        </h1>
                    </xsl:when>
                    <xsl:when test="regex-group(3)">
                        <ul>
                            <xsl:call-template name="list">
                                <xsl:with-param name="pString" 
                                    select="regex-group(3)"/>
                            </xsl:call-template>
                        </ul>
                    </xsl:when>
                </xsl:choose>
            </xsl:matching-substring>
            <xsl:non-matching-substring>
                <xsl:if test=".!='&#xA;'">
                    <p>
                        <xsl:call-template name="inline">
                            <xsl:with-param name="pString" 
                                select="normalize-space(.)"/>
                        </xsl:call-template>
                    </p>
                </xsl:if>
            </xsl:non-matching-substring>
        </xsl:analyze-string>
    </xsl:template>
    <xsl:template name="list">
        <xsl:param name="pString"/>
        <xsl:analyze-string select="$pString" regex="- (.*)&#xA;">
            <xsl:matching-substring>
                <li>
                    <xsl:call-template name="inline">
                        <xsl:with-param name="pString" 
                            select="regex-group(1)"/>
                    </xsl:call-template>
                </li>
            </xsl:matching-substring>
        </xsl:analyze-string>
    </xsl:template>
    <xsl:template name="inline">
        <xsl:param name="pString" select="."/>
        <xsl:analyze-string select="$pString" 
            regex="(__(.*)__)|(\*(.*)\*)|(&quot;(.*)&quot;\[(.*)\])">
            <xsl:matching-substring>
                <xsl:choose>
                    <xsl:when test="regex-group(1)">
                        <strong>
                            <xsl:value-of select="regex-group(2)"/>
                        </strong>
                    </xsl:when>
                    <xsl:when test="regex-group(3)">
                        <span>
                            <xsl:value-of select="regex-group(4)"/>
                        </span>
                    </xsl:when>
                    <xsl:when test="regex-group(5)">
                        <a href="{regex-group(7)}">
                            <xsl:value-of select="regex-group(6)"/>
                        </a>
                    </xsl:when>
                </xsl:choose>
            </xsl:matching-substring>
            <xsl:non-matching-substring>
                <xsl:value-of select="."/>
            </xsl:non-matching-substring>
        </xsl:analyze-string>
    </xsl:template>
</xsl:stylesheet>