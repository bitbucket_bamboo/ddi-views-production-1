<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:uml="http://www.omg.org/spec/UML/20110701" 
    xmlns:xmi="http://www.omg.org/spec/XMI/20110701" xmlns:ddifunc="ddi:functions"
    xmlns:xhtml="http://www.w3.org/1999/xhtml" exclude-result-prefixes="ddifunc uml xmi"
    version="2.0">
	
	    <xsl:import href="../Util/reStructure.xsl"/>
	
	    <!-- params -->
    <xsl:param name="filepath" select="'file:///C:/Work/output/'"/>
	
	<xsl:variable name="stylesheetVersion">1.0.0</xsl:variable>


	<xsl:template match="xmi:XMI">
        <protocol>
            <xsl:comment>
				<xsl:text>This file was created by field-fillings version </xsl:text>
				<xsl:value-of select="$stylesheetVersion"/>
			</xsl:comment>
			<classes><xsl:value-of select="count(//packagedElement[@xmi:type='uml:Class'])"/></classes>
			<xsl:result-document href="{$filepath}ClassesWithoutDefinition.txt">
				<xsl:apply-templates select="//packagedElement[@xmi:type='uml:Class']" mode="classDoc">
					<xsl:with-param name="context">||Definition</xsl:with-param>
				</xsl:apply-templates>
			</xsl:result-document>
			<xsl:result-document href="{$filepath}ClassesWithoutExamples.txt">
				<xsl:apply-templates select="//packagedElement[@xmi:type='uml:Class']" mode="classDoc">
					<xsl:with-param name="context">||Examples</xsl:with-param>
				</xsl:apply-templates>
			</xsl:result-document>
			<xsl:result-document href="{$filepath}ClassesWithoutExplanatoryNotes.txt">
				<xsl:apply-templates select="//packagedElement[@xmi:type='uml:Class']" mode="classDoc">
					<xsl:with-param name="context">||Explanatory notes</xsl:with-param>
				</xsl:apply-templates>
			</xsl:result-document>
			<xsl:result-document href="{$filepath}ClassesWithoutSynonyms.txt">
				<xsl:apply-templates select="//packagedElement[@xmi:type='uml:Class']" mode="classDoc">
					<xsl:with-param name="context">||Synonyms</xsl:with-param>
				</xsl:apply-templates>
			</xsl:result-document>
			<xsl:result-document href="{$filepath}ClassesWithoutDDI3.2mapping.txt">
				<xsl:apply-templates select="//packagedElement[@xmi:type='uml:Class']" mode="classDoc">
					<xsl:with-param name="context">||DDI 3.2 mapping</xsl:with-param>
				</xsl:apply-templates>
			</xsl:result-document>
			<xsl:result-document href="{$filepath}ClassesWithoutRDFmapping.txt">
				<xsl:apply-templates select="//packagedElement[@xmi:type='uml:Class']" mode="classDoc">
					<xsl:with-param name="context">||RDF mapping</xsl:with-param>
				</xsl:apply-templates>
			</xsl:result-document>
			<xsl:result-document href="{$filepath}ClassesWithoutGSIMmapping.txt">
				<xsl:apply-templates select="//packagedElement[@xmi:type='uml:Class']" mode="classDoc">
					<xsl:with-param name="context">||GSIM mapping</xsl:with-param>
				</xsl:apply-templates>
			</xsl:result-document>
		</protocol>
	</xsl:template>
	
	<xsl:template match="packagedElement" mode="classDoc">
		<xsl:param name="context"/>
		<xsl:variable name="text">
			<xsl:call-template name="docExtract">
				<xsl:with-param name="partName"><xsl:value-of select="$context"/></xsl:with-param>
				<xsl:with-param name="text"><xsl:value-of select="ownedComment/body"/></xsl:with-param>
			</xsl:call-template>
		</xsl:variable>
		<xsl:if test="$text=''">
			<xsl:value-of select="@name"/>
			<xsl:text>
</xsl:text>
		</xsl:if>
	</xsl:template>
	
</xsl:stylesheet>